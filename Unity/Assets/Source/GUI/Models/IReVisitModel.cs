using FF.ReVisit.GameLogic;

namespace FF.ReVisit.GUI.Models
{
    public interface IReVisitModel
    {
        IReVisitData Data { get; }

        void Initialize();

        //void Add(Category category);
        //event Action<Category> CategoryAddedEvent;
    }
}