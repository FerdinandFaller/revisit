using FF.ReVisit.GameLogic;
using Version = FF.ReVisit.GameLogic.Version;

namespace FF.ReVisit.GUI.Models
{
    public class ReVisitModel : IReVisitModel
    {
        public IReVisitData Data { get; private set; }

        private readonly Version _appVersion;


        public ReVisitModel(Version appVersion, IReVisitData data)
        {
            Data = data;
            _appVersion = appVersion;
        }

        public void Initialize()
        {
            //DeSerializeEverything();
            Data.CheckAndUpdate(_appVersion);
        }
    }
}