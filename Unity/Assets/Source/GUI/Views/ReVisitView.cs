using System;
using FF.ReVisit.GUI.Controllers;
using FF.ReVisit.GUI.Models;
using UnityEngine;

namespace FF.ReVisit.GUI.Views
{
    public class ReVisitView : MonoBehaviour
    {
        protected IReVisitController Controller;
        protected IReVisitModel Model;


        public virtual void Initialize(IReVisitController controller, IReVisitModel model)
        {
            if (controller == null) throw new ArgumentNullException("controller");
            if (model == null) throw new ArgumentNullException("model");

            Controller = controller;
            Model = model;
        }
    }
}