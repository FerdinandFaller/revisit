﻿using FF.ReVisit.GameLogic;
using FF.ReVisit.GUI.Controllers;
using FF.ReVisit.GUI.Models;
using UnityEngine;

namespace FF.ReVisit.GUI
{
    public class MVC : MonoBehaviour
    {
        //public LogoView LogoView;


        void Start()
        {
            // TODO: Data should be globally accessible
            var data = new JSONPrefsReVisitData();

            var model = new ReVisitModel(UIVersion.AppVersion, data);

            var args = new ReVisitController.ReVisitControllerArgs
                       {
                           //CategoriesView = CategoriesView,
                       };
            var controller = new ReVisitController(model, args);
        }
    }
}