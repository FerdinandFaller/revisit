using System;
using FF.ReVisit.GUI.Models;

namespace FF.ReVisit.GUI.Controllers
{
    public class ReVisitController : IReVisitController
    {
        public struct ReVisitControllerArgs
        {
            //public LogoView LogoView;
        }

        public IReVisitModel Model;

        //public LogoView LogoView;


        public ReVisitController(IReVisitModel model, ReVisitControllerArgs args)
        {
            if (model == null) throw new ArgumentNullException("model");

            Model = model;

            //LogoView = args.LogoView;
            //LogoView.Initialize(this, model);

            Model.Initialize();
        }
    }
}