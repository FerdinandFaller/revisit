﻿using System.Collections;
using UnityEngine;

namespace FF.ReVisit.GameLogic
{
    public class Location : MonoBehaviour
    {
        public GameObject Magnet;

        private LocationInfo _location;
        private LocationInfo _savedLocation;
        private bool _isPosSaved;
        private Vector2 _dirToSavedPos;
        private float _angleToSavedPos;


        // Longitude = x
        // Latitude = y

        void Start()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        void Update()
        {
            StartCoroutine_Auto(UpdateLocation());

            if (_isPosSaved)
            {
                var current = (new Vector2(_location.longitude, _location.latitude)) * 1000;
                var saved = (new Vector2(_savedLocation.longitude, _savedLocation.latitude)) * 1000;

                _dirToSavedPos = (saved - current).normalized;

                var magnetDir = new Vector2(Magnet.transform.up.x, Magnet.transform.up.y);

                _angleToSavedPos = Mathf.Cos(Vector2.Dot(_dirToSavedPos, magnetDir));
            }
        }

        void OnGUI()
        {
            UnityEngine.GUI.Label(new Rect(0,0,500,500), "Altitude: " + _location.altitude +
                                             "\nLatitude: " + _location.latitude +
                                             "\nLongitude " + _location.longitude +
                                             "\nTimestamp: " + _location.timestamp +
                                             "\nStatus: " + Input.location.status +
                                             "\nEnabled by User? : " + Input.location.isEnabledByUser + 
                                             "\nHorizAccuracy: " + _location.horizontalAccuracy +
                                             "\nVertAccuracy: " + _location.verticalAccuracy +
                                             "\nSavedLatitude: " + _savedLocation.latitude +
                                             "\nSavedLongitude " + _savedLocation.longitude +
                                             "\nDir: " + _dirToSavedPos +
                                             "\nAngle To SavedPos: " + _angleToSavedPos);

            if (UnityEngine.GUI.Button(new Rect(200, 0, 100, 100), "Save Position"))
            {
                _savedLocation = _location;
                _isPosSaved = true;
            }

            if (UnityEngine.GUI.Button(new Rect(300, 0, 100, 100), "Update"))
            {
                StartCoroutine_Auto(UpdateLocation());
            }
        }

        void OnDisable()
        {
            Input.location.Stop();
        }

        private IEnumerator UpdateLocation()
        {
            // First, check if user has location service enabled
		    if (!Input.location.isEnabledByUser)
			    yield return null;

            var status = Input.location.status;
            if (status == LocationServiceStatus.Stopped || status == LocationServiceStatus.Failed)
            {
                // Start service before querying location
                Input.location.Start(0,0);
            }

		    // Wait until service initializes
		    var maxWait = 20;
		    while (Input.location.status
		           == LocationServiceStatus.Initializing && maxWait > 0) {
			    yield return new WaitForSeconds (1);
			    maxWait--;
		    }

		    // Service didn't initialize in 20 seconds
		    if (maxWait < 1) 
            {
			    print ("Timed out");
			    yield return null;
		    }

		    // Connection has failed
		    if (Input.location.status == LocationServiceStatus.Failed) {
			    print ("Unable to determine device location");
			    yield return null;
		    }

		    // Access granted and location value could be retrieved
		    else
		    {
		        _location = Input.location.lastData;
		    }

		    // Stop service if there is no need to query location updates continuously
		    //Input.location.Stop ();
        }
    }
}
