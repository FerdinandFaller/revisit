namespace FF.ReVisit.GameLogic
{
    public interface IReVisitData
    {
        Version DataVersion { get; }


        void CheckAndUpdate(Version appVersion);

        // Read
        // Write
        // Delete
    }
}