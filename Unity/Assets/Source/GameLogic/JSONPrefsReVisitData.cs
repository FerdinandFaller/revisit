using FF.IO;

namespace FF.ReVisit.GameLogic
{
    public class JSONPrefsReVisitData : ReVisitData
    {
        public JSONPrefsReVisitData()
        {
            //JSONPrefs.DeleteAll();
        }

        protected override Version ReadDataVersion()
        {
            return new Version(JSONPrefs.GetInt("DataVersion-Major", 0), JSONPrefs.GetInt("DataVersion-Minor", 0), JSONPrefs.GetInt("DataVersion-Revision", 0));
        }

        protected override void WriteDataVersion()
        {
            JSONPrefs.SetInt("DataVersion-Major", DataVersion.Major);
            JSONPrefs.SetInt("DataVersion-Minor", DataVersion.Minor);
            JSONPrefs.SetInt("DataVersion-Revision", DataVersion.Revision);
        }

        protected override void UpdateData(Version appVersion)
        {
            if (DataVersion.Major == 0 && DataVersion.Minor == 0)
            {
                AddDefaultData();
                return;
            }

            if (appVersion < new Version(1, 0, 0))
            {
                return;
            }

            // Add aditional Versions and their conversions here
        }

        protected override void AddDefaultData()
        {
            // Here we can always add the most recent version of the default Data, because this method is only used when the app is launched for the first time
        }
    }
}