using UnityEngine;

namespace FF.ReVisit.GameLogic
{
    public abstract class ReVisitData : IReVisitData
    {
        public Version DataVersion { get; private set; }


        protected ReVisitData()
        {
            
        }

        public virtual void CheckAndUpdate(Version appVersion)
        {
            DataVersion = ReadDataVersion();

            Debug.Log("ReVisitData - CheckAndUpdate. AppVersion: " + appVersion + ", DataVersion: " + DataVersion);

            if (appVersion > DataVersion)
            {
                UpdateData(appVersion);

                DataVersion = appVersion;
                WriteDataVersion();
            }
        }

        // Read
        // Write
        // Delete

        protected abstract Version ReadDataVersion();
        protected abstract void WriteDataVersion();
        protected abstract void UpdateData(Version appVersion);
        protected abstract void AddDefaultData();
    }
}